# Wildcount :: Backend

## Web site
* https://wildcount.iot.imag.fr for Grafana
* https://wildcount-n.iot.imag.fr for NodeRED

## Layout
* configuration: contains containers' configurations
* data: contains containers' data (rw)
* backups: contains backups of the databases and messages log
* docker: contains the build of the containers
* screenshots: contains screenshots of the dashboards

## Ports
* nodered : TBD
* grafana : TBD
* influxdb : TBD

## Database
The name is `lorawan`

## Operations

```
# launch the composition
docker-compose up -d

# list the containers of the composition
docker-compose ps

# follow the logs of the containers
docker-compose logs -f

# stop the composition
docker-compose stop

# start the composition
docker-compose start

# destroy all the containers of the composition
docker-compose down
```

## TODO
