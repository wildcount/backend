# LoRa Device profiles
Profiles pour le réseau LoRaWAN de [CampusIoT](https://lns.campusiot.imag.fr/).

Onglet CODEC

Decode : pour recevoir une transmission du capteur au serveur
Encode : pour préparer une transmission du serveur au capteur