/*
 * Copyright 2021: Université Grenoble Alpes, GINP
 * 
 * Authors:
 *  - Didier Donsez
 *  - Tom Graugnard
 *  - Louis De Gaudenzi
 */


var FPORT_DN_GET_INFO = 1;
var FPORT_DN_GET_COUNTERS_PAGES = 16;
var FPORT_DN_GET_TX_ON_CLASS = 64;
var FPORT_DN_SET_TX_ON_CLASS = 65;

function Encode_DN_INFO(obj, variables) {
    let buffer = new ArrayBuffer(5);
    let tab = new Uint8Array(buffer);

    var pos = 0;
    tab[pos] = obj.flags >>> 8;
    pos += 1;
    tab[pos] = obj.flags;
    pos += 1;
    tab[pos] = obj.period_for_info_tx;
    pos += 1;
    tab[pos] = obj.period_for_counters_tx;
    pos += 1;
    tab[pos] = obj.period_between_next_photo;
    return buffer;
}

function Encode_DN_COUNTERS_PAGES(obj, variables) {
    let buffer = new ArrayBuffer(2);
    let tab = new Uint8Array(buffer);

    let pos = 0;
    tab[pos] = obj.mask_of_pages_to_send;
    pos++;
    tab[pos] = obj.counter_abstract;
    return buffer;
}

function Encode_DN_GET_TX_ON_CLASS(obj, variables) {
    return [];
}

function Encode_DN_SET_TX_ON_CLASS(obj, variables) {
    let buffer;
    if (variables.number_of_classes <= 51) {
        let size = Math.ceil(variables.number_of_classes / 8);
        buffer = new ArrayBuffer(size);
        let tab = new Uint8Array(buffer);

        for (let index = 0; index < variables.number_of_classes; index++) {
            let tabi = Math.floor(index / 8);
            tab[tabi] = tab[tabi] << 1;
            tab[tabi] |= obj.flag[index];
        }
    }
    return buffer;
}

// Encode encodes the given object into an array of bytes.
//  - fPort contains the LoRaWAN fPort number
//  - obj is an object, e.g. {"temperature": 22.5}
//  - variables contains the device variables e.g. {"calibration": "3.5"} (both the key / value are of type string)
// The function must return an array of bytes, e.g. [225, 230, 255, 0]
function Encode(fPort, obj, variables) {
    switch (fPort) {
        case FPORT_DN_GET_INFO:
            return Encode_DN_INFO(obj, variables);
        case FPORT_DN_GET_COUNTERS_PAGES:
            return Encode_DN_COUNTERS_PAGES(obj, variables);
        case FPORT_DN_GET_TX_ON_CLASS:
            return Encode_DN_GET_TX_ON_CLASS(obj, variables);
        case FPORT_DN_SET_TX_ON_CLASS:
            return Encode_DN_SET_TX_ON_CLASS(obj, variables);
        default:
            return [];
    }
}
