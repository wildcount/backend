/*
 * Wildcount
 * Copyright 2021-2023: Université Grenoble Alpes, GINP
 * 
 * Authors:
 *  - Didier Donsez
 *  - Alexis Rollin
 *  - Tom Graugnard
 * 
 * Version: 2023-03-01 for firmware https://gitlab.com/wildcount/firmware-spresense (Commit 7cc2587e)
 */



var FPORT_UP_INFO = 1;
var FPORT_UP_LAST_COUNTERS = 2;
var FPORT_UP_COUNTERS = 3;
var FPORT_UP_LAST_COUNTERS_PAGE_0 = 4;
var FPORT_UP_LAST_COUNTERS_PAGE_1 = 5;
var FPORT_UP_LAST_COUNTERS_PAGE_2 = 6;
var FPORT_UP_LAST_COUNTERS_PAGE_3 = 7;
var FPORT_UP_COUNTERS_PAGE_0 = 16;
var FPORT_UP_COUNTERS_PAGE_1 = 17;
var FPORT_UP_COUNTERS_PAGE_2 = 18;
var FPORT_UP_COUNTERS_PAGE_3 = 19;
var FPORT_UP_COUNTERS_ABSTRACT = 32;
var FPORT_UP_TX_ON_CLASS = 64;

var NUMBER_OF_CLASSES = 64;

// Value used for the conversion of the position from DMS to decimal.
var MaxNorthPosition = 8388607; // 2^23 - 1
var MaxEastPosition = 8388607; // 2^23 - 1

function readUInt16BE(buf, offset) {
    offset = offset >>> 0;
    return (buf[offset] << 8) | buf[offset + 1];
}

function readUInt32BE(buf, offset) {
    offset = offset >>> 0;

    return (buf[offset] * 0x1000000) +
        ((buf[offset + 1] << 16) |
            (buf[offset + 2] << 8) |
            buf[offset + 3]);
}

function readUInt8BE(buf, offset) {
    offset = offset >>> 0;
    return (buf[offset]);
}

function getBit(byte, position) {
    return (byte >>> position) & 1;
}

function Decode_UP_INFO(bytes, variables) {

    var o = {};
    var size = bytes.length;

    var flags = readUInt16BE(bytes, 0);
    if ((flags & 0x4000) !== 0) { // bit 14 indicates some problem for the sdcard.
        o.sdcard_problem = true;
    }
    if ((flags & 0x8000) !== 0) { // bit 15 indicates the detected intrusion of the endpoint.
        o.intrusion = true;
    }

    var pos = 2;

    if (pos + 4 <= size && (flags & 0x0001) !== 0) { //bit 0 indicates the occurrence of fw_version.
        o.fw_version = readUInt32BE(bytes, pos);
        pos += 4;
    }

    if (pos + 1 <= size && (flags & 0x0002) !== 0) { //bit 1 indicates the occurrence of number_of_classes.
        o.number_of_classes = bytes[pos];
        pos += 1;
    }

    if (pos + 2 <= size && (flags & 0x0004) !== 0) { //bit 2 indicates the occurrence of periods.
        o.period_for_info_tx = bytes[pos++];
        o.period_for_counters_tx = bytes[pos++];
    }

    o.fCnt_of_the_last_reset = readUInt8BE(bytes, pos);
    pos += 1;

    if (pos + 5 <= size && (flags & 0x0008) !== 0) { //bit 4 indicates the occurrence of temperature, humidity, pressure.
        o.temperature = readUInt16BE(bytes, pos) / 10.0;
        pos += 2;
        o.humidity = bytes[pos++];
        pos += 1;
        o.pressure = readUInt16BE(bytes, pos) / 10.0;
        pos += 2;
    }

    if (pos + 11 <= size && (flags & 0x0010) !== 0) { //bit 5 indicates the occurrence of latitude, longitude, altitude, gps_quality.
        // Extract latitude.
        var Latitude = (readUInt32BE(bytes, pos) >> 8) & 0x7FFF;
        Latitude = Latitude * 90 / MaxNorthPosition;
        o.latitude = Math.round(Latitude * 1000000) / 1000000;
        pos += 4;

        // Extract longitude.
        var Longitude = (readUInt32BE(bytes, pos) >> 8);
        Longitude = Longitude * 180 / MaxEastPosition;
        o.longitude = Math.round(Longitude * 1000000) / 1000000;
        pos += 4;

        o.altitude = readUInt16BE(bytes, pos);
        pos += 2;

        o.gps_quality = bytes[pos++];
    }

    if (pos + 4 <= size && (flags & 0x0020) !== 0) { //bit 6 indicates the occurrence of remaining_MBytes_on_sdcard and number_of_photos.
        // Extract remaining_MBytes_on_sdcard.
        var Remaining = readUInt16BE(bytes, pos);
        o.remaining_MBytes_on_sdcard = Remaining;
        pos += 2;

        // Extract number_of_photos.
        var Number_of_photos = readUInt16BE(bytes, pos);
        o.number_of_photos = Number_of_photos;
        pos += 2;
    }

    if (pos + 2 <= size && (flags & 0x0040) !== 0) { // bit 7 indicates the occurrence of number_of_infered_classes.
        // Extract number_of_infered_classes.
        var Number_of_infered_classes = readUInt16BE(bytes, pos);
        o.remaining_of_infered_classes = Number_of_infered_classes;
        pos += 2;
    }

    if (pos + 2 <= size && (flags & 0x0080) !== 0) { // bit 8 indicates the occurrence of battery_level.
        // Extract battery_level.
        var Battery_level = readUInt16BE(bytes, pos);
        o.battery_level = Battery_level;
        pos += 2;
    }

    if (pos + 1 <= size && (flags & 0x0100) !== 0) { // bit 9 indicates the occurrence of confirmation of last_counters and last_counters_page messages.
        o.confirmation_last_counters = true;
    }

    if (pos + 1 <= size && (flags & 0x0200) !== 0) { // bit 10 indicates the occurrence of confirmation of counters and counters_page messages.
        o.confirmation_counters = true;
    }

    o.watchdog_reboot = bytes[pos];
    pos += 1;

    return o;
}

function Decode_UP_LAST_COUNTERS(bytes, variables) {
    var size = bytes.length;

    var counters = [];

    var bytes_pos = 0;
    var counter_pos = 0;

    var t = {};
    t.detected_classes = bytes[bytes_pos++];
    counters[counter_pos] = t;
    counter_pos += 1;

    while (bytes_pos < size) { // reading <class(8b);counter(8b)> pairs

        var o = {};
        o.class = bytes[bytes_pos++];
        o.counter = bytes[bytes_pos++];
        counters[counter_pos] = o;
        counter_pos += 1;
    }

    return counters;
}

function Decode_UP_COUNTERS(bytes, variables) {
    var size = bytes.length;

    var counters = {};
    var bytes_pos = 0;
    var counter_pos = 0;

    var n_classes = readUInt8BE(bytes, bytes_pos);
    bytes_pos += 1;


    var n = 1;

    while (bytes_pos < size) {
        counters["class" + n] = readUInt16BE(bytes, bytes_pos);
        n += 1;
        bytes_pos += 2;
        counter_pos += 1;
    }
    return counters;
}

function Decode_UP_COUNTERS_array(bytes, variables) {
    var size = bytes.length;

    var counters = [];
    var bytes_pos = 0;
    var counter_pos = 0;

    var n_classes = readUInt8BE(bytes, bytes_pos);
    bytes_pos += 1;


    var n = 1;

    while (bytes_pos < size) {
        var o = {};
        o.class = n;
        n += 1;
        o.counter = readUInt16BE(bytes, bytes_pos);
        bytes_pos += 2;
        counters[counter_pos] = o;
        counter_pos += 1;
    }
    return counters;
}

function Decode_UP_LAST_COUNTERS_PAGE_0(bytes, variables) {
    var size = NUMBER_OF_CLASSES / 4;

    var counters = [];

    var pos = 0;

    while (pos < size) { // reading <counter(8b)>

        var o = {};
        o.class = pos; // class with id pos
        o.counter = bytes[pos];
        counters[pos] = o;
        pos += 1;
    }

    return counters;
}

function Decode_UP_LAST_COUNTERS_PAGE_1(bytes, variables) {
    var size = NUMBER_OF_CLASSES / 4;

    var counters = [];

    var pos = 0;

    while (pos < size) { // reading <counter(8b)>

        var o = {};
        o.class = size + pos; // class with id size+pos
        o.counter = bytes[pos];
        counters[pos] = o;
        pos += 1;
    }

    return counters;
}

function Decode_UP_LAST_COUNTERS_PAGE_2(bytes, variables) {
    var size = NUMBER_OF_CLASSES / 4;

    var counters = [];

    var pos = 0;

    while (pos < size) { // reading <counter(8b)>

        var o = {};
        o.class = 2 * size + pos; // class with id 2*size+pos
        o.counter = bytes[pos];
        counters[pos] = o;
        pos += 1;
    }

    return counters;
}

function Decode_UP_LAST_COUNTERS_PAGE_3(bytes, variables) {
    var size = NUMBER_OF_CLASSES / 4;

    var counters = [];

    var pos = 0;

    while (pos < size) { // reading <counter(8b)>

        var o = {};
        o.class = 3 * size + pos; // class with id 3*size+pos
        o.counter = bytes[pos];
        counters[pos] = o;
        pos += 1;
    }

    return counters;
}

function Decode_UP_COUNTERS_PAGE_0(bytes, variables) {
    var size = NUMBER_OF_CLASSES / 4;

    var counters = [];

    var pos = 0;
    var counter_pos = 0;

    while (pos < size * 2) { // reading <counter(16b)>

        var o = {};
        o.class = counter_pos;
        o.counter = readUInt16BE(bytes, pos);
        pos += 2;
        counters[counter_pos] = o;
        counter_pos += 1;
    }

    return counters;
}

function Decode_UP_COUNTERS_PAGE_1(bytes, variables) {
    var size = NUMBER_OF_CLASSES / 4;

    var counters = [];

    var pos = 0;
    var counter_pos = 0;

    while (pos < size * 2) { // reading <counter(16b)>

        var o = {};
        o.class = size + counter_pos;
        o.counter = readUInt16BE(bytes, pos);
        pos += 2;
        counters[counter_pos] = o;
        counter_pos += 1;
    }

    return counters;
}

function Decode_UP_COUNTERS_PAGE_2(bytes, variables) {
    var size = NUMBER_OF_CLASSES / 4;

    var counters = [];

    var pos = 0;
    var counter_pos = 0;

    while (pos < size * 2) { // reading <counter(16b)>

        var o = {};
        o.class = 2 * size + counter_pos;
        o.counter = readUInt16BE(bytes, pos);
        pos += 2;
        counters[counter_pos] = o;
        counter_pos += 1;
    }

    return counters;
}

function Decode_UP_COUNTERS_PAGE_3(bytes, variables) {
    var size = NUMBER_OF_CLASSES / 4;

    var counters = [];

    var pos = 0;
    var counter_pos = 0;

    while (pos < size * 2) { // reading <counter(16b)>

        var o = {};
        o.class = 3 * size + counter_pos;
        o.counter = readUInt16BE(bytes, pos);
        pos += 2;
        counters[counter_pos] = o;
        counter_pos += 1;
    }

    return counters;
}

function Decode_UP_COUNTERS_ABSTRACT(bytes, variables) {
    var o = {};

    var pos = 0;

    o.last_counter_page_0 = bytes[pos++];
    o.last_counter_page_1 = bytes[pos++];
    o.last_counter_page_2 = bytes[pos++];
    o.last_counter_page_3 = bytes[pos++];

    var Counter_page_0 = readUInt16BE(bytes, pos);
    o.counter_page_0 = Counter_page_0;
    pos += 2;

    var Counter_page_1 = readUInt16BE(bytes, pos);
    o.counter_page_1 = Counter_page_1;
    pos += 2;

    var Counter_page_2 = readUInt16BE(bytes, pos);
    o.counter_page_2 = Counter_page_2;
    pos += 2;

    var Counter_page_3 = readUInt16BE(bytes, pos);
    o.counter_page_3 = Counter_page_3;
    pos += 2;

    var Remaining = readUInt16BE(bytes, pos);
    o.remaining_MBytes_on_sdcard = Remaining;
    pos += 2;

    var Number_of_photos = readUInt16BE(bytes, pos);
    o.number_of_photos = Number_of_photos;
    pos += 2;

    var Number_of_infered_classes = readUInt16BE(bytes, pos);
    o.number_of_infered_classes = Number_of_infered_classes;
    pos += 2;


    var Number_of_tx_on_infered_classes = readUInt16BE(bytes, pos);
    o.number_of_tx_on_infered_classes = Number_of_tx_on_infered_classes;
    pos += 2;

    return o;
}

function Decode_UP_TX_ON_CLASS(bytes, variables) {

    var classes = [];
    var pos = 0;

    if (NUMBER_OF_CLASSES <= 51) {
        for (pos = 0; pos < NUMBER_OF_CLASSES; pos += 8) {
            for (var i = 0; i < 8; i++) {
                classes[pos + i] = getBit(bytes[pos], 7 - i);
            }
        }
    }

    return classes;
}

// Decode decodes an array of bytes into an object.
//  - fPort contains the LoRaWAN fPort number
//  - bytes is an array of bytes, e.g. [225, 230, 255, 0]
//  - variables contains the device variables e.g. {"calibration": "3.5"} (both the key / value are of type string)
// The function must return an object, e.g. {"temperature": 22.5}
function Decode(fPort, bytes, variables) {

    switch (fPort) {
        case FPORT_UP_INFO:
            return Decode_UP_INFO(bytes, variables);
        case FPORT_UP_LAST_COUNTERS:
            return Decode_UP_LAST_COUNTERS(bytes, variables);
        case FPORT_UP_COUNTERS:
            return Decode_UP_COUNTERS(bytes, variables);
        case FPORT_UP_LAST_COUNTERS_PAGE_0:
            return Decode_UP_LAST_COUNTERS_PAGE_0(bytes, variables);
        case FPORT_UP_LAST_COUNTERS_PAGE_1:
            return Decode_UP_LAST_COUNTERS_PAGE_1(bytes, variables);
        case FPORT_UP_LAST_COUNTERS_PAGE_2:
            return Decode_UP_LAST_COUNTERS_PAGE_2(bytes, variables);
        case FPORT_UP_LAST_COUNTERS_PAGE_3:
            return Decode_UP_LAST_COUNTERS_PAGE_3(bytes, variables);
        case FPORT_UP_COUNTERS_PAGE_0:
            return Decode_UP_COUNTERS_PAGE_0(bytes, variables);
        case FPORT_UP_COUNTERS_PAGE_1:
            return Decode_UP_COUNTERS_PAGE_1(bytes, variables);
        case FPORT_UP_COUNTERS_PAGE_2:
            return Decode_UP_COUNTERS_PAGE_2(bytes, variables);
        case FPORT_UP_COUNTERS_PAGE_3:
            return Decode_UP_COUNTERS_PAGE_3(bytes, variables);
        case FPORT_UP_COUNTERS_ABSTRACT:
            return Decode_UP_COUNTERS_ABSTRACT(bytes, variables);
        case FPORT_UP_TX_ON_CLASS:
            return Decode_UP_TX_ON_CLASS(bytes, variables);
        default:
            break;
    }
    return null;
}
