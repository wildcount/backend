/*
 * Class A LoRa Device profile
 * Authors:
 *  - Thais Bourret
 *  - Romain Pigret-Cadou
 */


var FPORT_DN_GET_INFO = 1;
var FPORT_DN_GET_COUNTERS_PAGES = 16;
var FPORT_DN_GET_TX_ON_CLASS = 64;
var FPORT_DN_SET_TX_ON_CLASS = 65;


// Encode encodes the given object into an array of bytes.
//  - fPort contains the LoRaWAN fPort number
//  - obj is an object, e.g. {"temperature": 22.5}
//  - variables contains the device variables e.g. {"calibration": "3.5"} (both the key / value are of type string)
// The function must return an array of bytes, e.g. [225, 230, 255, 0]
function Encode(fPort, obj, variables) {
    switch (fPort) {
        default:
            return [];
    }
}
