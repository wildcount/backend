/*
 * Class A LoRa Device profile
 * Authors:
 *  - Thais Bourret
 *  - Romain Pigret-Cadou
 */



var FPORT_UP_INFO = 1;
var FPORT_UP_LAST_COUNTERS = 2;
var FPORT_UP_COUNTERS = 3;
var FPORT_UP_LAST_COUNTERS_PAGE_0 = 4;
var FPORT_UP_LAST_COUNTERS_PAGE_1 = 5;
var FPORT_UP_LAST_COUNTERS_PAGE_2 = 6;
var FPORT_UP_LAST_COUNTERS_PAGE_3 = 7;
var FPORT_UP_COUNTERS_PAGE_0 = 16;
var FPORT_UP_COUNTERS_PAGE_1 = 17;
var FPORT_UP_COUNTERS_PAGE_2 = 18;
var FPORT_UP_COUNTERS_PAGE_3 = 19;
var FPORT_UP_COUNTERS_ABSTRACT = 32;
var FPORT_UP_TX_ON_CLASS = 64;

var NUMBER_OF_CLASSES = 10; // Please define same value into the sensor board and decide to which animal each index is referring
var MAX_NUMBER_OF_CLASSES_PAGES = 25; // Fixed

// Value used for the conversion of the position from DMS to decimal.
var MaxNorthPosition = 8388607; // 2^23 - 1
var MaxEastPosition = 8388607; // 2^23 - 1

function readUInt16BE(buf, offset) {
    offset = offset >>> 0;
    return (buf[offset] << 8) | buf[offset + 1];
}

function readUInt32BE(buf, offset) {
    offset = offset >>> 0;

    return (buf[offset] * 0x1000000) +
        ((buf[offset + 1] << 16) |
            (buf[offset + 2] << 8) |
            buf[offset + 3]);
}

function readUInt8BE(buf, offset) {
    offset = offset >>> 0;
    return (buf[offset]);
}

function getBit(byte, position) {
    return (byte >>> position) & 1;
}

/**
 * Uplink page i counters since last counters tx
 * @param {7} bytes 
 * @param {*} variables 
 * @returns json last counters into page selected 
 */
function Decode_UP_LAST_COUNTERS_PAGE_i(bytes, variables, page) {
    var size = MAX_NUMBER_OF_CLASSES_PAGES;

    if (size > MAX_NUMBER_OF_CLASSES_PAGES) {
        size = MAX_NUMBER_OF_CLASSES_PAGES; //25
    }

    var start = MAX_NUMBER_OF_CLASSES_PAGES * page;
    var size = NUMBER_OF_CLASSES - start;

    var counters = [];
    for (var pos = 0; pos < size; pos++) { // reading <counter(8b)>
        var o = {};
        o.class = start + pos; // class with id pos
        o.counter = (((bytes[pos * 2] & 0xFF) << 8) | (bytes[pos * 2 + 1] & 0xFF)); // to byte to made one uint16, supposed positive
        counters[pos] = o;
    }
    return counters;
}



// Decode decodes an array of bytes into an object.
//  - fPort contains the LoRaWAN fPort number
//  - bytes is an array of bytes, e.g. [225, 230, 255, 0]
//  - variables contains the device variables e.g. {"calibration": "3.5"} (both the key / value are of type string)
// The function must return an object, e.g. {"temperature": 22.5}
function Decode(fPort, bytes, variables) {

    switch (fPort) {
        case FPORT_UP_LAST_COUNTERS_PAGE_0:
            return Decode_UP_LAST_COUNTERS_PAGE_i(bytes, variables, 0);
        case FPORT_UP_LAST_COUNTERS_PAGE_1:
            return Decode_UP_LAST_COUNTERS_PAGE_i(bytes, variables, 1);
        case FPORT_UP_LAST_COUNTERS_PAGE_2:
            return Decode_UP_LAST_COUNTERS_PAGE_i(bytes, variables, 2);
        case FPORT_UP_LAST_COUNTERS_PAGE_3:
            return Decode_UP_LAST_COUNTERS_PAGE_i(bytes, variables, 3);
        default:
            break;
    }
    return null;
}
