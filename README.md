# Wildcount :: Backend

Backend for processing, storing and visualizing Wildcount data.

## Codec

* Payload decoders and encoders for Chirpstack are available [here](docker).

## Backend

* Nodered for ETL from MQTT
* InfluxDB for time-serie storage
* Grafana for Dataviz
* Docker compose for DevOps

## Dashboards

![Grafana](./images/dashboard-01.jpg)
![Grafana](./images/dashboard-02.jpg)

* class 1 - blaireau
* class 2 - chamois
* class 3 - chevreuil
* class 4 - chien
* class 5 - ecureuil
* class 6 - lievre
* class 7 - loup
* class 8 - mustelide
* class 9 - renard
* class 10 - sanglier
* class 11 - vide
