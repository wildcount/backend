$(function() {
    class Test {
        constructor(fPort, data, object, variables) {
            this.fPort = fPort;
            this.data = data;
            this.object = object;
            this.variables = variables;
        }
    }

    var tests = [
        new Test(
            1,
            [0x02,0x01,0xFF,0x35,0x00],
            {
                'flags': 513,
                'period_for_info_tx': 255,
                'period_for_counters_tx': 53,
                'period_between_next_photo': 0
            },
            null
        ),
        new Test(
            1,
            [0xFF,0xFF,0xFF,0xFF,0xFF],
            {'flags':65535,'period_for_info_tx':255,'period_for_counters_tx':255,'period_between_next_photo':255},
            null
        ),
        new Test(
            1,
            [0x00,0x00,0x00,0x00,0x00],
            {'flags':0,'period_for_info_tx':0,'period_for_counters_tx':0,'period_between_next_photo':0},
            null
        ),
        new Test(
            1,
            [0x01,0x02,0x03,0x04,0x05],
            {'flags':258,'period_for_info_tx':3,'period_for_counters_tx':4,'period_between_next_photo':5},
            {'bonjour':"test"}
        ),
        new Test(
            16,
            [0x93,0x63],
            {'mask_of_pages_to_send':147,'counter_abstract':99},
            null
        ),
        new Test(
            16,
            [0x00,0x00],
            {'mask_of_pages_to_send':0,'counter_abstract':0},
            null
        ),
        new Test(
            16,
            [0xFF,0xFF],
            {'mask_of_pages_to_send':255,'counter_abstract':255},
            null
        ),
        new Test(
            16,
            [0x13,0xA9],
            {'mask_of_pages_to_send':19,'counter_abstract':169},
            null
        ),
        new Test(
            16,
            [0x90,0x90],
            {'mask_of_pages_to_send':144,'counter_abstract':144},
            null
        ),
        new Test(
            64,
            [],
            {},
            null
        ),
        new Test(
            65,
            [0x74,0x29],
            {'flag':[0,1,1,1,0,1,0,0,0,0,1,0,1,0,0,1]},
            {'number_of_classes':16}
        ),
        new Test(
            65,
            [0x74],
            {'flag':[0,1,1,1,0,1,0,0]},
            {'number_of_classes':8}
        ),
        new Test(
            65,
            [0x00,0x00],
            {'flag':[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]},
            {'number_of_classes':16}
        ),
        new Test(
            65,
            [0xFF,0xFF],
            {'flag':[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]},
            {'number_of_classes':16}
        ),
        new Test(
            65,
            [0x01],
            {'flag':[1]},
            {'number_of_classes':1}
        ),
        new Test(
            65,
            [0x12,0x06],
            {'flag':[0,0,0,1,0,0,1,0,0,0,1,1,0]},
            {'number_of_classes':13}
        ),
    ];
    
    function compare(a, b) {
        for (let i = 0; i < a.length; i++) {
            if ((a[i] !== b[i]))
                return false;
        }
        return true;
    }

    tests.forEach(test => {
        var encoded = new Uint8Array(Encode(test.fPort, test.object, test.variables));
        var bArray = new Uint8Array(test.data);

        var result = compare(encoded, bArray);

        console.assert(result, encoded, bArray);

        if(result)
            console.log("OK", encoded, bArray)
    });
});