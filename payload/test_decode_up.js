$(function() {
    class Test {
        constructor(fPort, data, object, variables) {
            this.fPort = fPort;
            this.data = data;
            this.object = object;
            this.variables = variables;
        }
    }

    var tests = [
        new Test(
            1,
            [0xC1,0xDF,0x00,0x00,0x00,0x01,0x00,0x0B,0x03,0x04,0x05,0x09,0x03,0x00,0xFF,0x20,0x09,0x60,0xF0,0x00,0x00,0x80,0x00,0x64,0x00,0x32,0x58],
            {"battery_level": 50,
            "fCnt_of_the_last_reset": 1289,
            "fw_version": 1,
            "humidity": 255,
            "intrusion": true,
            "number_of_classes": 0,
            "number_of_photos": 0,
            "period_between_next_photo": 4,
            "period_for_counters_tx": 3,
            "period_for_info_tx": 11,
            "pressure": 820.1,
            "remaining_MBytes_on_sdcard": 24816,
            "remaining_of_infered_classes": 32768,
            "remaining_of_tx_on_infered_classes": 25600,
            "sdcard_problem": true,
            "temperature": 76.8},
            null
        ),
        new Test(
            1,
            [0x00,0x02,0x11],
            {"number_of_classes":17},
            null
        ),
        new Test(
            2,
            [0x0E,0x02,0x03,0x0A,0x01,0x01,0x05,0x04],
            {"14":2,"3":10,"1":1,"5":4},
            null
        ),
        new Test(
            2,
            [0x00,0x00,0x00,0xFF],
            {"0":255},
            null
        ),
        new Test(
            3,
            [0x0D,0x00,0x22,0x0F,0x00,0x05,0x01,0x00,0xFF,0x04,0x00,0x44,0x09,0x00,0x00],
            {"13":34,"15":5,"1":255,"4":68,"9":0},
            null
        ),
        new Test(
            3,
            [0x0D,0x00,0x22,0x0F,0x00,0x05,0x01,0x01,0xFF,0x04,0x00,0x44,0x09,0x00,0x00],
            {"13":34,"15":5,"1":511,"4":68,"9":0},
            null
        ),
        new Test(
            3,
            [],
            {},
            null
        ),
        new Test(
            4,
            [0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,0x0F],
            {"0":0,"1":1,"2":2,"3":3,"4":4,"5":5,"6":6,"7":7,"8":8,"9":9,"10":10,"11":11,"12":12,"13":13,"14":14,"15":15},
            null
        ),
        new Test(
            4,
            [0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,0x0F,0x28,0x7F],
            {"0":0,"1":1,"2":2,"3":3,"4":4,"5":5,"6":6,"7":7,"8":8,"9":9,"10":10,"11":11,"12":12,"13":13,"14":14,"15":15},
            null
        ),
        new Test(
            5,
            [0x04,0x0D,0x05,0x05,0x0F,0x03,0x0C,0x08,0x01,0x0D,0x00,0x00,0x00,0x00,0x00,0x00],
            {"16":4,"17":13,"18":5,"19":5,"20":15,"21":3,"22":12,"23":8,"24":1,"25":13,"26":0,"27":0,"28":0,"29":0,"30":0,"31":0},
            null
        ),
        new Test(
            6,
            [0x00,0x00,0x00,0x00,0x00,0x0F,0x0F,0x0F,0x02,0x0B,0x0B,0x05,0x06,0x01,0x00,0x0E],
            {"32":0,"33":0,"34":0,"35":0,"36":0,"37":15,"38":15,"39":15,"40":2,"41":11,"42":11,"43":5,"44":6,"45":1,"46":0,"47":14},
            null
        ),
        new Test(
            7,
            [0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00],
            {"48":0,"49":0,"50":0,"51":0,"52":0,"53":0,"54":0,"55":0,"56":0,"57":0,"58":0,"59":0,"60":0,"61":0,"62":0,"63":0},
            null
        ),
        new Test(
            16,
            [0x00,0x01,0x00,0x23,0x00,0x45,0x00,0x67,0x00,0x89,0x00,0xAB,0x00,0xCD,0x00,0xEF,0x00,0x01,0x00,0x23,0x00,0x45,0x00,0x67,0x00,0x89,0x00,0xAB,0x00,0xCD,0x00,0xEF],
            {"0":1,"1":35,"2":69,"3":103,"4":137,"5":171,"6":205,"7":239,"8":1,"9":35,"10":69,"11":103,"12":137,"13":171,"14":205,"15":239},
            null
        ),
        new Test(
            17,
            [0x00,0xA9,0x00,0x14,0x00,0x73,0x00,0x8B,0x00,0xBC,0x00,0x34,0x00,0x03,0x00,0x03,0x00,0x04,0x00,0x01,0x00,0x05,0x00,0x03,0x00,0x0B,0x00,0xA3,0x00,0x39,0x00,0x99],
            {"16":169,"17":20,"18":115,"19":139,"20":188,"21":52,"22":3,"23":3,"24":4,"25":1,"26":5,"27":3,"28":11,"29":163,"30":57,"31":153},
            null
        ),
        new Test(
            18,
            [0x00,0x01,0x00,0x02,0x00,0x03,0x00,0x04,0x00,0x05,0x00,0x06,0x00,0x07,0x00,0x08,0x00,0x09,0x00,0x0A,0x00,0x0B,0x00,0x0C,0x00,0x0D,0x00,0x0E,0x00,0x0F,0x00,0x00],
            {"32":1,"33":2,"34":3,"35":4,"36":5,"37":6,"38":7,"39":8,"40":9,"41":10,"42":11,"43":12,"44":13,"45":14,"46":15,"47":0},
            null
        ),
        new Test(
            19,
            [0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00],
            {"48":0,"49":0,"50":0,"51":0,"52":0,"53":0,"54":0,"55":0,"56":0,"57":0,"58":0,"59":0,"60":0,"61":0,"62":0,"63":0},
            null
        ),
        new Test(
            32,
            [0x07,0x07,0x07,0x07,0x00,0x08,0x00,0x24,0x00,0xA0,0x00,0x1F,0x00,0xE4,0x00,0x17,0x00,0x08,0x00,0xFF],
            {"last_counter_page_0":7,"last_counter_page_1":7,"last_counter_page_2":7,"last_counter_page_3":7,"counter_page_0":8,"counter_page_1":36,"counter_page_2":160,"counter_page_3":31,"remaining_MBytes_on_sdcard":228,"number_of_photos":23,"number_of_infered_classes":8,"number_of_tx_on_infered_classes":255},
            null
        ),
        new Test(
            32,
            [0x07,0x07,0x07,0x07,0x00,0x08,0x00,0x24,0x00,0xA0,0x00,0x1F,0x00,0xE4,0x00,0x17,0x00,0x08,0x00,0xFF,0x23],
            {"last_counter_page_0":7,"last_counter_page_1":7,"last_counter_page_2":7,"last_counter_page_3":7,"counter_page_0":8,"counter_page_1":36,"counter_page_2":160,"counter_page_3":31,"remaining_MBytes_on_sdcard":228,"number_of_photos":23,"number_of_infered_classes":8,"number_of_tx_on_infered_classes":255},
            null
        ),
        new Test(
            64,
            [0x74,0x29],
            {"flag":[0,1,1,1,0,1,0,0,0,0,1,0,1,0,0,1]},
            null
        ),
        new Test(
            64,
            [0x00,0x00],
            {"flag":[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]},
            null
        ),
        new Test(
            64,
            [0xFF,0xFF],
            {"flag":[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]},
            null
        ),
        new Test(
            64,
            [0x80],
            {"flag":[1,0,0,0,0,0,0,0]},
            null
        ),
        new Test(
            64,
            [0x12,0x34],
            {"flag":[0,0,0,1,0,0,1,0,0,0,1,1,0,1,0,0]},
            null
        ),
    ];
    
    function compare(a, b) {
        for(var p in a) {
            if(a[p] !== b[p]) {
                return false;
            }
        }
        return true;
    }

    tests.forEach(test => {
        if(test.fPort === 64) {
            NUMBER_OF_CLASSES = test.object.flag.length;
        }
        var decoded = Decode(test.fPort, test.data, test.variables);
        var object = test.object;

        var result; 

        if(test.fPort === 64) {
            result = compare(decoded.flag, object.flag)
        }
        else {
            result = compare(decoded, object);
        }

        console.assert(result, decoded, object);

        if(result)
            console.log("OK", decoded, object)
    });
});